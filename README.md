### Nextjs boilerplate

#### Why

because nextjs does not come with zero config for general features like

* Styling solution
* File loader
* SSR config
* assets prefix
* folder structure
* in style url
* sub directory support

#### Install dependencies

`npm install`

#### Run

`npm run dev`

#### Build and Export 

`npm run build`

`npm run export`
