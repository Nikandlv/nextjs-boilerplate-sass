import Wrapper from '../components/Wrapper';
export default function Index() {
	return (
		  <Wrapper>
			<h1>Welcome to NextJS boilerplate</h1>
			<a href="https://github.com/nikandlv/nextjs-boilerplate-sass">Github repo</a>
		  </Wrapper>

	);
}
